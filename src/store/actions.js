import fbApp from '../firebase'
const db = fbApp.database()

export default {
  setUser({commit, state},user) {
    commit('SET_USER',user) 
  },
  unSetUser({commit,state}) {
    commit('UNSET_USER')
  },
  fetchTasks({commit, state}){
    // Turning on the loading indicator
    commit('LOADING_ON')
    db.ref(`tasks/${state.user.uid}`).once('value')
      .then(snapshot => {
        // Laading up any available tasks in local state 
        commit('SET_TASKS', snapshot.val()) 
        // Turning off the loading indicator
        commit('LOADING_OFF')
      }, 
      () => commit('TOAST', 'Failed to retrieve tasks'))
  },
  addTask({ commit, state }, newItem) {
    const newRef = db.ref(`tasks/${state.user.uid}`).push({
      title: newItem
    })
    newRef.then(commit('ADD_TASK', { newItem, key: newRef.key }), () => commit('TOAST', 'Failed to add task'))
  },
  deleteTask({ commit, state }, key) {
    const ref = db.ref(`tasks/${state.user.uid}/${key}`).remove()
    ref.then(() => { commit('REMOVE_TASK',key) }, () => commit('TOAST','Failed to remove'))
  },
  signOut({ commit }) {
    fbApp.auth().signOut()
      .then(commit('UNSET_USER'), () => commit('TOAST', 'Failed to sign out'))
  },
  loadingOn({commit}) {
    commit('LOADING_ON')
  },
  loadingOff({commit}) {
    commit('LOADING_OFF')
  }
}